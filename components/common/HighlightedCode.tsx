import Prism from "prismjs";
import {PropsWithChildren, useEffect} from "react";
import "prismjs/themes/prism-tomorrow.css";
import "prismjs/components/prism-typescript"
import {Box} from "@mui/material";

export const HighlightedCode = (props: PropsWithChildren<{
  language: string,
  fontSize?: string
}>) => {
  const { language, fontSize, children } = props;

  useEffect(() => {
    Prism.highlightAll();
  }, []);

  return (
    <Box sx={{
      fontSize: fontSize ?? "1.5vh"
    }}>
      <pre>
        <code className={`language-${language}`}>
          {children}
        </code>
      </pre>
    </Box>
  );
}