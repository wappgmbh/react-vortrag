import {createContext, PropsWithChildren, useCallback, useContext, useEffect, useState} from "react";
import {useRouter} from "next/router";
import {SxProps} from "@mui/system";
import {Box, styled, Theme} from "@mui/material";

const slidePaths: (string | [string, number])[] = [
  "/",
  "/typescript/overview",
  "/typescript/types",
  "/typescript/interfaces",
  "/typescript/undefined",
  "/typescript/functions",
  "/typescript/casting", // 15 min
  "/react/overview",
  "/react/firstComponent",
  "/react/properties",
  "/react/if",
  "/react/stateExample",
  "/react/stateTheory",
  "/react/hooks", // +20 min / 35 min
  "/react/todoExample", // +5 min / 40 min
  "/react/effectExample1",
  "/react/effectTheory",
  "/react/ref",
  "/react/wrapper",
  "/react/context", // +15 min / 55 min
  "/react/quiz1",
  "/react/quiz2",
  "/react/quiz3", // 70 min
  "/mui/overview",
  "/mui/components",
  "/mui/componentsTheory",
  "/mui/notistack",
  "/mui/dialog",
  ["/mui/styling", 5],
  "/mui/theme", // 90 min
  "/nextjs/overview",
  ["/nextjs/structure", 8],
  "/nextjs/example",
  "/nextjs/routing", // 100 min
  "/debugging/overview",
  "/debugging/renderCycle",
  "/debugging/renderTooFew",
  "/debugging/renderTooFewExample",
  "/debugging/renderTooOften",
  "/debugging/renderTooOftenExample",
  "/debugging/renderTooMuch",
  "/debugging/memo",
  "/debugging/useCallback",
  "/debugging/useMemo", // 115 min
  "/libraries/overview",
  "/libraries/apiGenerator",
  "/libraries/swr",
  "/libraries/dataTable",
  "/libraries/i18n",
  "/libraries/other",
]

interface Slide {
  path: string;
  stateCount: number;
}

const slidePathToSlide = (path: string | [string, number]): Slide => {
  if (typeof path === "string") {
    return {
      path,
      stateCount: 1
    }
  } else {
    return {
      path: path[0],
      stateCount: path[1]
    }
  }
}

const slides = slidePaths.map(slidePathToSlide);

const SlideStateContext = createContext(0);

export const useSteps = () => {
  const slideState = useContext(SlideStateContext);

  return {
    visibleAfter: (step: number, otherStyles?: SxProps<Theme>): SxProps<Theme> => ({
      ...otherStyles,
      visibility: slideState >= step ? "visible" : "hidden"
    }),
    visibleIn: (step: number, otherStyles?: SxProps<Theme>): SxProps<Theme> => ({
      ...otherStyles,
      visibility: slideState == step ? "visible": "hidden"
    }),
    highlightAt: (step: number): SxProps<Theme> => ({
      color: slideState === step ? "#00a5e0" : undefined
    })
  }
}

export const Overlays = (props: PropsWithChildren<{}>) => {

  return (
    <Box sx={{}}>
      {props.children}
    </Box>
  )
}

export const Columns = styled(Box)(() => ({
  display: "flex",
  "& > *": {
    flexGrow: 1,
    margin: "0.5vh",
    width: 0
  }
}))

export const Overlay = (props: PropsWithChildren<{
  step: number
}>) => {
  const { step } = props;
  const slideState = useContext(SlideStateContext);

  return (
    <>{slideState === step && props.children}</>
  );
}

export const Presentation = (props: PropsWithChildren<{}>) => {
  const { children } = props;

  const router = useRouter();
  const path = router.pathname

  const [slide, setSlide] = useState(0);
  const [slideState, setSlideState] = useState(0);

  useEffect(() => {
    const slideStr = window.localStorage.getItem("slide");
    if (slideStr) {
      let slide = parseInt(slideStr, 10);
      if (slide >= slides.length) {
        slide = slides.length - 1;
      }
      setSlide(slide);
    } else {
      setSlide(0)
    }
  }, []);

  const updateSlide = useCallback((slide: number) => {
    setSlide(slide);
    setSlideState(0);
    window.localStorage.setItem("slide", slide.toString(10));
    router.push(slides[slide].path);
  }, [router])

  useEffect(() => {
    const pathSlide = slides.findIndex(s => s.path === path);
    if (!pathSlide || slide === pathSlide) {
      return
    }
    updateSlide(pathSlide);
  }, [path, slide, updateSlide]);

  useEffect(() => {
    const onKeyPress = (ev: KeyboardEvent) => {
      switch (ev.key) {
        case "ArrowLeft":
          if (slideState > 0) {
            setSlideState(slideState - 1)
          } else if (slide > 0) {
            updateSlide(slide - 1);
          }
          break;
        case "ArrowRight":
          if (slideState + 1 < slides[slide].stateCount) {
            setSlideState(slideState + 1);
          } else if (slide + 1 < slides.length) {
            updateSlide(slide + 1);
          }
          break;
        case "ArrowUp":
          updateSlide(0);
      }
    }
    document.addEventListener('keyup', onKeyPress)
    return () => {
      document.removeEventListener('keyup', onKeyPress);
    }
  });

  return (
    <SlideStateContext.Provider value={slideState}>
      {children}
    </SlideStateContext.Provider>
  );
}