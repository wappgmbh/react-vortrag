

export const Sandbox = (props: {
  src: string,
  height?: number,
}) => {
  const { src, height } = props;

  return (
    <iframe style={{ height: `${height ?? 80}vh` }} src={src} />
  )
}