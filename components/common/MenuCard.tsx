import {Box, Card} from "@mui/material";
import Link from 'next/link'
import Image from "next/image"

export const MenuCard = (props: {
  backgroundColor: string,
  color: string,
  name: string,
  logo?: string,
  link?: string
}) => {
  const { name, color, backgroundColor, logo, link } = props;

  return (
    <Link href={link ?? "#"}>
      <Card sx={{
        backgroundColor,
        color,
        display: "flex",
        flexDirection: "column",
        flexGrow: 1,
        justifyContent: "space-between",
        margin: "2vh",
        padding: "8% 10%",
        boxSizing: "border-box"
      }}>
        <Box sx={{
          fontSize: "4vh"
        }}>
          {name}
        </Box>
        <Box sx={{
          textAlign: "right"
        }}>
          {logo &&
            <Box sx={{ height: "100px", position: "relative" }}>
              <Image src={logo} alt="Logo" layout="fill" objectFit="contain" objectPosition="right"  />
            </Box>
          }
        </Box>
      </Card>
    </Link>
  )
}