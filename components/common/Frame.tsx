import {PropsWithChildren} from "react";
import {Box, createTheme, styled, ThemeProvider} from "@mui/material";
import {Presentation} from "./Presentation";

const theme = createTheme({
  palette: {
    text: {
      primary: "white"
    }
  },
  typography: {
    fontSize: 24
  }
})

export const Narrow = styled(Box)(() => ({
  margin: "0 auto",
  width: "70%"
}))

export const Frame = (props: PropsWithChildren<{}>) => {
  const { children} = props;

  return (
    <ThemeProvider theme={theme}>
      <Box sx={{
        color: "white",
        display: "flex",
        fontSize: "3.5vh",
        flexDirection: "column",
        margin: "auto",
        minHeight: "100vh",
        padding: "4vh",
        width: "90vw"
      }}>
        <Presentation>
          {children}
        </Presentation>
      </Box>
    </ThemeProvider>
  )
}