import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Routing: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Routing</Typography>
      <Sandbox src="https://codesandbox.io/embed/next-routing-zpewy?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Routing;