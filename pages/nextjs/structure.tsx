import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Columns, Overlay, Overlays, useSteps} from "../../components/common/Presentation";


export const Structure: NextPage = () => {
  const steps = useSteps();

  return (
    <>
      <Typography variant="h1">Projektstruktur</Typography>
      <Columns>
        <Box>
          <Typography variant="h2">Orderstruktur</Typography>
          <ul>
            <li>pages
              <ul>
                <Box component="li" sx={steps.highlightAt(1)}>index.tsx</Box>
                <li>subpath
                  <ul>
                    <Box component="li" sx={steps.highlightAt(2)}>index.tsx</Box>
                    <Box component="li" sx={steps.highlightAt(3)}>subpage.tsx</Box>
                  </ul>
                </li>
                <Box component="li" sx={steps.highlightAt(4)}>_app.tsx</Box>
              </ul>
            </li>
            <Box component="li" sx={steps.highlightAt(5)}>components</Box>
            <li>public
              <ul>
                <Box component="li" sx={steps.highlightAt(6)}>logo.svg</Box>
              </ul>
            </li>
            <li>styles
              <ul>
                <Box component="li" sx={steps.highlightAt(7)}>globals.css</Box>
              </ul>
            </li>
          </ul>
        </Box>
        <Overlays>
          <Overlay step={1}>
            <ul>
              <li>Muss eine Komponente vom Typ <code>NextPage</code> als Default-Export haben</li>
              <li>Wird angezeigt, wenn auf <code>/</code> navigiert wird</li>
            </ul>
          </Overlay>
          <Overlay step={2}>
            <ul>
              <li>Wird angezeigt, wenn auf <code>/subpath</code> navigiert wird</li>
            </ul>
          </Overlay>
          <Overlay step={3}>
            <ul>
              <li>Wird angezeigt, wenn auf <code>/subpath/subpage</code> navigiert wird</li>
            </ul>
          </Overlay>
          <Overlay step={4}>
            <ul>
              <li>Hier kann ein Rahmen für alle Seiten definiert werden</li>
            </ul>
          </Overlay>
          <Overlay step={5}>
            <ul>
              <li>Hier sollten alle Komponenten definiert werden, die keine eigenenständigen Seiten sind</li>
            </ul>
          </Overlay>
          <Overlay step={6}>
            <ul>
              <li>Alles unter <code>public</code> wird 1:1 in die kompilierte Webseite übertragen </li>
              <li>Kann in der Anwendung mit <code>&lt;img src=&quot;/logo.svg&quot; /&gt;</code> referenziert werden</li>
            </ul>
          </Overlay>
          <Overlay step={7}>
            <ul>
              <li>Hier können globale und Modul-Styles definiert werden</li>
            </ul>
          </Overlay>
        </Overlays>
      </Columns>
    </>
  )
}

export default Structure;