import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Example: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Beispiel-Anwendung</Typography>
      <Sandbox src="https://codesandbox.io/embed/test-next-app-biy9i?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Example;