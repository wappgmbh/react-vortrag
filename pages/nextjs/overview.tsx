import {NextPage} from "next";
import {Box, Typography} from "@mui/material";


export const Overview: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Next.js</Typography>
      <Box>
        <ul>
          <li>Framework, das auf React aufbaut</li>
          <li>Eingebautes Routing</li>
          <li>Compiletime Rendering</li>
          <li>Weitere Performance-Optimierungen</li>
          <li>Projekt-Erstellung mit <code>npx create-next-app --ts</code></li>
        </ul>
        <Box sx={{
          display: "flex",
          justifyContent: "center"
        }}>
          <Box sx={{
            backgroundColor: "white",
            padding: "2vh"
          }}>
            <img style={{ height: "30vh" }} src="/img/nextjs-logo.png" alt="" />
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default Overview;