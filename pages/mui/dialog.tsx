import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Dialog: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Dialoge</Typography>
      <Sandbox src="https://codesandbox.io/embed/dialogs-q3jhi?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Dialog;