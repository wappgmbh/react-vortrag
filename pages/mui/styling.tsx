import {NextPage} from "next";
import {Box, Rating, styled, Table, TableBody, TableCell, TableHead, TableRow, Typography} from "@mui/material";
import {Overlays, Overlay, useSteps, Columns} from "../../components/common/Presentation";
import {HighlightedCode} from "../../components/common/HighlightedCode";

const sxExample = `import {Box} from "@mui/material";

export const Example = (props: PropsWithChildren<{
  n: number
}>) => {
  const { n, children } = props;

  return (
    <Box sx={{
      margin: "5px",
      color: n > 5 ? "red" : "white",
      "& > *": {
        border: "1px solid black"
      }
    }}>
      {children}
    </Box>
  )
}
`;

export const styledExample = `import {Box} from "@mui/material";

const ChildBox = styled(Box)(({ theme }) => ({
  color: theme.palette.primary.main,
  flexGrow: 1
}))

export const Example = () => {
  return (
    <Box sx={{
      display: "flex"
    }}>
      <ChildBox>A</ChildBox>
      <ChildBox>B</ChildBox>
    </Box>
  )
}
`;

const ListBox = styled(Box)(() => ({
  fontSize: "2.5vh"
}))

export const Overview: NextPage = () => {
  const steps = useSteps();

  return (
    <>
      <Typography variant="h1">Styling</Typography>
      <Box>
        <Table size="small" sx={{
          "& *": {
            fontSize: "2.5vh !important"
          }
        }}>
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell sx={steps.visibleAfter(1)}><code>sx</code>-Property</TableCell>
              <TableCell sx={steps.visibleAfter(2)}><code>styled</code>-Funktion</TableCell>
              <TableCell sx={steps.visibleAfter(3)}>Module CSS Datei</TableCell>
              <TableCell sx={steps.visibleAfter(4)}>Globale CSS Datei</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>Betroffene Elemente einfach identifizierbar</TableCell>
              <TableCell sx={steps.visibleAfter(1)}><Rating value={5} /></TableCell>
              <TableCell sx={steps.visibleAfter(2)}><Rating value={4} /></TableCell>
              <TableCell sx={steps.visibleAfter(3)}><Rating value={3} /></TableCell>
              <TableCell sx={steps.visibleAfter(4)}><Rating value={1} /></TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Don&apos;t Repeat Yourself</TableCell>
              <TableCell sx={steps.visibleAfter(1)}><Rating value={3} /></TableCell>
              <TableCell sx={steps.visibleAfter(2)}><Rating value={4} /></TableCell>
              <TableCell sx={steps.visibleAfter(3)}><Rating value={4} /></TableCell>
              <TableCell sx={steps.visibleAfter(4)}><Rating value={5} /></TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Benötigtes React-Wissen</TableCell>
              <TableCell sx={steps.visibleAfter(1)}><Rating value={2} /></TableCell>
              <TableCell sx={steps.visibleAfter(2)}><Rating value={2} /></TableCell>
              <TableCell sx={steps.visibleAfter(3)}><Rating value={3} /></TableCell>
              <TableCell sx={steps.visibleAfter(4)}><Rating value={5} /></TableCell>
            </TableRow>
          </TableBody>
        </Table>
        <Overlays>
          <Overlay step={1}>
            <Columns>
              <HighlightedCode language="typescript">
                {sxExample}
              </HighlightedCode>
              <ListBox>
                <ul>
                  <li>Styles sind auf 1 Usage einer Komponente beschränkt</li>
                  <li>Verknüpfte Styles für Element direkt erkennbar</li>
                  <li>Wiederverwendbarkeit durch Wiederverwendung der Komponente</li>
                  <li>Nicht aus &quot;@mui/system&quot; importieren, sondern aus &quot;@mui/material&quot;</li>
                </ul>
              </ListBox>
            </Columns>
          </Overlay>
          <Overlay step={2}>
            <Columns>
              <HighlightedCode language="Typescript">
                {styledExample}
              </HighlightedCode>
              <ListBox>
                <ul>
                  <li>Styles sind auf 1 Komponente beschränkt</li>
                  <li>Praktisch, wenn Komponente mehrfach innerhalb einer Komponente vorkommt</li>
                </ul>
              </ListBox>
            </Columns>
          </Overlay>
          <Overlay step={3}>
            <ListBox>
              <ul>
                <li>Styles sind auf 1 Komponente beschränkt</li>
                <li>Datei, die in eigenem Bereich liegt und mit &quot;<code>module.css</code>&quot; endet</li>
                <li>Muss in Komponente mit &quot;<code>import styles from &lsquo;../styles/Home.module.css&lsquo;</code> importiert werden</li>
                <li>Enthält klassisches CSS</li>
                <li>Mit <code>&lt;div className={"{styles.classname}"}&gt;</code> kann ein Style angewendet werden</li>
              </ul>
            </ListBox>
          </Overlay>
          <Overlay step={4}>
            <ListBox>
              <ul>
                <li>Styles sind global</li>
                <li>CSS-Datei, die im <code>styles</code>-Ordner liegt</li>
                <li>Wird in der Top-Level Komponente mit &quot;<code>import &quot;./globals.css&quot;</code> importiert</li>
              </ul>
            </ListBox>
          </Overlay>
        </Overlays>
      </Box>
    </>
  )
}

export default Overview;