import {NextPage} from "next";
import {Box, Typography} from "@mui/material";


export const Overview: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Material UI</Typography>
      <Box>
        <ul>
          <li><u>Das</u> Styling-Framework für React</li>
          <li>Orientiert sich an der Material Designsprache von Google</li>
          <li>Viele vorgefertigte Komponenten (auch für Apps)</li>
          <li>Enthält auch einige Icons</li>
        </ul>
        <Box sx={{
          display: "flex",
          justifyContent: "center"
        }}>
          <img style={{ height: "40vh" }} src="/img/mui.png" alt="" />
        </Box>
      </Box>
    </>
  )
}

export default Overview;