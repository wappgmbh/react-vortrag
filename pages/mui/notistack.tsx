import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Notistack: NextPage = () => {
  return (
    <>
      <Typography variant="h1">notistack</Typography>
      <Sandbox src="https://codesandbox.io/embed/mui-inotify-c4019?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Notistack;