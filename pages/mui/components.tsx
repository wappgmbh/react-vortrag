import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Components: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Vorgefertigte Komponenten</Typography>
      <Sandbox src="https://codesandbox.io/embed/mui-components-wlyte?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Components;