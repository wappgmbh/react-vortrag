import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Theme: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Theme</Typography>
      <Sandbox src="https://codesandbox.io/embed/mui-theme-wtoqb?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Theme;