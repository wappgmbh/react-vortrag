import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import { Narrow } from "../../components/common/Frame";


export const ComponentsTheory: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">Komponenten</Typography>
      <Box>
        <ul>
          <li>Labels und Inputs nicht mittels IDs verknüpfen
            <ul>
              <li>Verhindert wiederverwendbarkeit</li>
            </ul>
          </li>
          <li><code>Box</code> sollte anstelle von <code>div</code> verwendet werden
            <ul>
              <li>Ermöglicht Styling</li>
            </ul>
          </li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default ComponentsTheory;