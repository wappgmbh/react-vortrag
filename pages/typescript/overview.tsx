import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import { Narrow } from "../../components/common/Frame";


export const Overview: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">Typescript</Typography>
      <Box>
        <ul>
          <li>Javascript mit Typisierung</li>
          <li>Entwickelt von Microsoft</li>
          <li>Typen müssen manchmal annotiert werden, können aber häufig inferiert werden</li>
          <li>Autovervollständigung</li>
          <li>Compilezeit-Checks</li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default Overview;