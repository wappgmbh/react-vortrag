import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";

export const Functions: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Funktionen</Typography>
      <Box>
        <ul>
          <li>Typen für Funktionen sind an Definition angelehnt</li>
          <li><code>(param1Name: param1Typ, param2Name: param2Typ) =&gt; returnType</code></li>
        </ul>
      </Box>
      <Sandbox height={60} src="https://codesandbox.io/embed/functions-ju64p?fontsize=14&hidenavigation=1&theme=dark&view=editor" />
    </>
  )
}

export default Functions;