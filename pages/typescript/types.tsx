import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";
import {Columns} from "../../components/common/Presentation";

export const Types: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Typen</Typography>
      <Columns>
        <Box>
          <ul>
            <li>Primitive Typen
              <ul>
                <li><code>boolean, number, string</code></li>
              </ul>
            </li>
            <li>Vereinigungen mit <code>|</code></li>
            <li>Typen können mit <code>type</code> deklariert werden</li>
            <li>Strings sind auch Typen</li>
            <li>Unbekannte Typen
              <ul>
                <li><code>any</code>, <code>unknown</code></li>
              </ul>
            </li>
          </ul>
        </Box>
        <Sandbox src="https://codesandbox.io/embed/types-t8lv4?fontsize=14&hidenavigation=1&theme=dark&view=editor" />
      </Columns>
    </>
  )
}

export default Types;