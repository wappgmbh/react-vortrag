import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";
import {Columns} from "../../components/common/Presentation";

export const Interfaces: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Interfaces</Typography>
      <Columns>
        <Box>
          <ul>
            <li>Definition einer Objektstruktur
              <ul>
                <li>Welche Felder mit welchen Typen gibt es?</li>
              </ul>
            </li>
            <li>Strukturelle Typisierung</li>
          </ul>
        </Box>
        <Sandbox src="https://codesandbox.io/embed/interfaces-ltvnd?fontsize=14&hidenavigation=1&theme=dark&view=editor" />
      </Columns>
    </>
  )
}

export default Interfaces;