import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";

export const Casting: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Casting!</Typography>
      <Box>
        <ul>
          <li>Typsystem existiert nur zur Compilezeit</li>
          <li>Casts können nicht überprüft werden</li>
          <li><code>!</code> castet null und undefined weg</li>
          <li>Unpassende casts mit <code>as unknown as T</code></li>
        </ul>
      </Box>
      <Sandbox height={50} src="https://codesandbox.io/embed/casts-w84im?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Casting;