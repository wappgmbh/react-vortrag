import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";
import {Columns} from "../../components/common/Presentation";

export const Undefined: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Null und Undefined</Typography>
      <Columns>
        <Box>
          <ul>
            <li>Für <code>null</code> und <code>undefined</code> gibt es Typen</li>
            <li>Häufig mit Union (<code>|</code>) und anderen Typen verwendet</li>
            <li>Optionale Felder in Objekten werden mit <code>?</code> gekennzeichnet</li>
          </ul>
        </Box>
        <Sandbox src="https://codesandbox.io/embed/undefined-9eqjl?fontsize=14&hidenavigation=1&theme=dark&view=editor" />
      </Columns>
    </>
  )
}

export default Undefined;