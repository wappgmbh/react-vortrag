import type { NextPage } from 'next'
import {Box, styled} from "@mui/material";
import {MenuCard} from "../components/common/MenuCard";
import {Narrow} from "../components/common/Frame";
import typescriptLogo from "../public/img/typescript-logo.svg"

const CardContainer = styled(Box)(() => ({
  display: "grid",
  flexGrow: 1,
  gridTemplateColumns: "1fr 1fr",
  gridTemplateRows: "1fr 1fr 1fr",
  width: "100%"
}))

const Home: NextPage = () => {
  return (
    <Narrow>
      <CardContainer>
        <MenuCard name="Typescript" backgroundColor="#3178c6" color="#ffffff" logo={typescriptLogo} link="/typescript/overview" />
        <MenuCard name="React" backgroundColor="#212121" color="#ffffff" logo="/img/react-logo.svg" link="/react/overview" />
        <MenuCard name="Material UI" color="#000000" backgroundColor="#ffffff" logo="/img/material-ui-logo.png" link="/mui/overview" />
        <MenuCard name="NextJS" color="#000000" backgroundColor="#f7f7f7" logo="/img/nextjs-logo.svg" link="/nextjs/overview" />
        <MenuCard name="Performance & Debugging" color="#000000" backgroundColor="#fded35" link="/debugging/overview" />
        <MenuCard name="Libraries" color="#ffffff" backgroundColor="#21a449" link="/libraries/overview" />
      </CardContainer>
    </Narrow>
  )
}

export default Home
