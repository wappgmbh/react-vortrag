import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Swr: NextPage = () => {
  return (
    <>
      <Typography variant="h1">SWR</Typography>
      <Sandbox src="https://codesandbox.io/embed/libraries-swr-e25g1?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Swr;