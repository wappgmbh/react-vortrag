import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";


export const Other: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">Andere Bibliotheken</Typography>
      <Box>
        <ul>
          <li>ReactQuill
            <ul>
              <li>WYSIWYG-Editor</li>
            </ul>
          </li>
          <li>html-react-parser
            <ul>
              <li>Rendern von HTML-Strings in einer Komponente</li>
              <li>z.B. Output von ReactQuill</li>
            </ul>
          </li>
          <li>react-lottie-player</li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default Other;