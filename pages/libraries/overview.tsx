import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";


export const Overview: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">Libraries und API</Typography>
      <Box>
        <ul>
          <li>OpenAPI Generator für Backend-Client</li>
          <li>SWR</li>
          <li>Dies und das
            <ul>
              <li>DataTable</li>
              <li>ReactQuill</li>
              <li>html-react-parser</li>
              <li>i18n</li>
              <li>react-lottie-player</li>
            </ul>
          </li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default Overview;