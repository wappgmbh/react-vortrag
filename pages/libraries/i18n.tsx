import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";
import {HighlightedCode} from "../../components/common/HighlightedCode";
import {Columns} from "../../components/common/Presentation";

const usage = `  const { t } = useTranslation();

  // ...

  <CircleButton
    to={isComplete ? "/questions/0" : undefined}
    label={t("personalDataForm.next")}
  />
`

const translationType = `type DeepKeys<T, S extends string> =
    T extends object
    ? S extends ${"`"}${"$"}{infer I1}.${"$"}{infer I2}${"`"}
        ? I1 extends keyof T
            ? ${"`"}${"$"}{I1}.${"$"}{DeepKeys<T[I1], I2>}${"`"}
            : keyof T & string
        : S extends keyof T
            ? ${"`"}${"$"}{S}${"`"}
            : keyof T & string
    : ""

function t<S extends string>(p: DeepKeys<typeof dict, S>)
  : GetDictValue<S, typeof dict> { /* impl */ }
`;

export const I18n: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">i18n</Typography>
      <Columns>
        <ul>
          <li>react-intl
            <ul>
              <li>Alt aber immer noch maintained</li>
              <li>Verbose</li>
            </ul>
          </li>
          <li>react-18next
            <ul>
              <li>Hook-Support</li>
              <li>Compilezeit-Check auf vorhandene Übersetzungen einbaubar</li>
            </ul>
          </li>
        </ul>
        <Box>
          <HighlightedCode language="typescript">
            {usage}
          </HighlightedCode>
          <HighlightedCode language="typescript">
            {translationType}
          </HighlightedCode>
        </Box>
      </Columns>
    </Narrow>
  )
}

export default I18n;