import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Columns} from "../../components/common/Presentation";
import {HighlightedCode} from "../../components/common/HighlightedCode";
import image from "../../public/img/datatable.png" ;
import Img from "next/image";

const code = `<DataTable<Optician>
  cacheKey={dataTableCacheKey}
  dataSupplier={api.getOpticians.bind(api)}
  columns={[
    {
      name: "name",
      label: "Name"
    },
    {
      name: "streetAndNumber",
      label: "Anschrift"
    }, {
      name: "zip",
      label: "PLZ"
    }, {
      name: "city",
      label: "Stadt"
    },
    {
      label: "",
      sortable: false,
      render: row => (
        <Box sx={{
          display: "flex",
          justifyContent: "end"
        }}>
          <IconButton component={Link} to={\`/optician/${"$"}{row.id}\`}>
            <img src="/img/table_edit.svg" alt="Bearbeiten" />
          </IconButton>
          <IconButton onClick={() => setDeleteDialogOptician(row)}>
            <img src="/img/table_delete.svg" alt="Löschen" />
          </IconButton>
        </Box>
      )
    }
  ]}
/>`

export const DataTable: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Data Table</Typography>
      <Columns>
        <Box sx={{
          "& pre": {
            margin: "0 !important"
          }
        }}>
          <HighlightedCode language="typescript" fontSize="1.3vh">
            {code}
          </HighlightedCode>
        </Box>
        <Box>
          <Img src={image} />
        </Box>
      </Columns>
    </>
  )
}

export default DataTable;