import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";
import Img from "next/image"
import apiAutocomplete from "../../public/img/api-autocomplete.png"


export const ApiGenerator: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">OpenAPI Generator</Typography>
      <Box>
        <ul>
          <li>Erzeugt Interfaces aus Dtos</li>
          <li>Generiert Klasse mit einer Methode pro Endpunkt</li>
          <li>Mittels eigener Fetch-Implementierung lassen sich Authentifizierung und Login-Redirects realisieren</li>
          <li>Klasse sollte in einer Wrapper-Komponente mit korrekter Backend-URL und ggf. Session-Token instanziiert werden und dann per Kontext verfügbar sein</li>
        </ul>
        <Img src={apiAutocomplete} />
      </Box>
    </Narrow>
  )
}

export default ApiGenerator;