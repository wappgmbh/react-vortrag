import '../styles/globals.css'
import type { AppProps } from 'next/app'
import {Frame} from "../components/common/Frame";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Frame>
      <Component {...pageProps} />
    </Frame>
  )
}

export default MyApp
