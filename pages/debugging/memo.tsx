import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";

export const Memo: NextPage = () => {
  return (
    <>
      <Typography variant="h1">memo()</Typography>
      <Sandbox src="https://codesandbox.io/embed/debugging-memo-e5u0i?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Memo;