import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";


export const RenderTooFew: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">React rendert zu wenig</Typography>
      <Box>
        &quot;Die Seite sollte sich ändern, aber es passiert nix&quot;
        <Typography variant="h3">Mögliche Ursachen</Typography>
        <ul>
          <li>Setter von <code>useState</code> nicht benutzt, sondern aktuellen Zustand verändert</li>
          <li>Setter von <code>useState</code> mit verändertem aktuellen Zustand aufgerufen
            <ul>
              <li>Setter muss immer mit einem neuen Objekt aufgerufen werden</li>
            </ul>
          </li>
          <li><code>useContext</code> in einer Komponente verwendet, die nicht unterhalb des Kontext-Providers liegt</li>
          <li>Hook-Dependency fehlt</li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default RenderTooFew;