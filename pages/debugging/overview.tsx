import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";


export const Overview: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">Debugging und Performance Optimierung</Typography>
      <Box>
        Häufige Probleme bei React-Seiten:
        <ul>
          <li>Es wird zu wenig gerendert
            <ul>
              <li>Z.B.: State wurde verändert, aber Seite wird nicht aktualisiert</li>
            </ul>
          </li>
          <li>Es wird zu häufig gerendert
            <ul>
              <li>Re-Render-Loop</li>
            </ul>
          </li>
          <li>Es wird zu viel gerendert
            <ul>
              <li>Nahezu vollständiger Seitenaufbau verlangsamt die Anwendung</li>
            </ul>
          </li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default Overview;