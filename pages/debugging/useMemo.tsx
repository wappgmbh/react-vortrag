import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";


export const useMemo: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">useMemo()</Typography>
      <Box>
        <ul>
          <li>Nicht verwechseln mit <code>memo()</code></li>
          <li>Ähnlich wie <code>useCallback()</code>, nur für Werte, die keine Funktionen sind</li>
          <li>Akzeptiert Funktion, die Wert berechnet</li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default useMemo;