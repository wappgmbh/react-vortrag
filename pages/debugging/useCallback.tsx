import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";

export const UseCallback: NextPage = () => {
  return (
    <>
      <Typography variant="h1">useCallback()</Typography>
      <Sandbox src="https://codesandbox.io/embed/debugging-usecallback-77fn4?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default UseCallback;