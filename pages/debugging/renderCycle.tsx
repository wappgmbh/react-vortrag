import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";


export const RenderCycle: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">React Render-Cycle</Typography>
      <Box>
        <ol>
          <li>Re-Rendering einer Komponente wird ausgelöst z.B. durch einen Hook
            <ul>
              <li>Setter von <code>useState</code> wird aufgerufen</li>
              <li>Von <code>useContext</code> genutzter Kontext wird verändert</li>
            </ul>
          </li>
          <li>Funktion, die Komponente definiert, wird erneut aufgerufen
            <ul>
              <li>Gilt auch für die Kind-Komponenten</li>
              <li>Ergebnis ist der Virtual DOM</li>
            </ul>
          </li>
          <li>React vergleicht Virtual DOM mit DOM</li>
          <li>React erstellt, löscht und updatet Elemente im DOM, die im Virtual DOM im Vergleich zum Document DOM zusätzlich da sind, fehlen oder anders sind</li>
        </ol>
      </Box>
    </Narrow>
  )
}

export default RenderCycle;