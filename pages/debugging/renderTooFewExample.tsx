import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";

export const RenderTooFewExample: NextPage = () => {
  return (
    <>
      <Typography variant="h1">React rendert zu wenig - Beispiel</Typography>
      <Sandbox src="https://codesandbox.io/embed/debugging-render-too-few-hxjuk?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default RenderTooFewExample;