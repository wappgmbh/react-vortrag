import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";

export const RenderTooOftenExample: NextPage = () => {
  return (
    <>
      <Typography variant="h1">React rendert zu häufig - Beispiel</Typography>
      <Sandbox src="https://codesandbox.io/embed/debugging-rerender-loop-bf0n3?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default RenderTooOftenExample;