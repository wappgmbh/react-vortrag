import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";


export const RenderTooMuch: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">React rendert zu viel</Typography>
      <Box>
        <Box>&quot;Eine Seite mit vielen Elementen reagiert langsam auf Eingaben&quot;</Box>
        <Box>&quot;In der Konsole steht <u>nicht</u> <code>maximum update depth exceeded</code>&quot;</Box>

        <Typography variant="h3">Mögliche Ursachen</Typography>
        <ul>
          <li>Bei jeder Eingabe (Checkbox anhaken, Buchstaben zu Textfeld hinzufügen) wird ein Großteil der Seite neu gerendert</li>
          <li>Nicht React-Spezifisches Problem
            <ul>
              <li>Warten auf Netzwerk-Request</li>
              <li>Komplizierte Berechnung in Typescript</li>
            </ul>
          </li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default RenderTooMuch;