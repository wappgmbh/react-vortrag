import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";


export const RenderTooOften: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">React rendert zu häufig</Typography>
      <Box>
        <Box>&quot;Die Seite reagiert immer langsamer und friert irgendwann (fast) komplett ein&quot;</Box>
        <Box>&quot;In der Konsole steht <code>maximum update depth exceeded</code>&quot;</Box>

        <Typography variant="h3">Mögliche Ursachen</Typography>
        <ul>
          <li>Hook verändert seine eigene Dependency</li>
          <li>Hook verändert Dependency eines anderen Hooks, der wiederum Dependency des Ursprungs-Hooks verändert</li>
          <li>...</li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default RenderTooOften;