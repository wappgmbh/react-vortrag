import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const TodoExample: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Listen-Beispiel</Typography>
      <Sandbox src="https://codesandbox.io/embed/todoexample-8ib05?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default TodoExample;