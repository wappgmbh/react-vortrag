import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";


export const StateTheory: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">Zustände</Typography>
      <Box>
        <ul>
          <li>Zustand kann primitiv oder komplex (Objekt) sein</li>
          <li>Parameter für Setter kann 2 Formen haben:
            <ul>
              <li>Neuer Zustand</li>
              <li>Funktion, die neuen Zustand aus altem berechnet</li>
            </ul>
          </li>
          <li>Neuer Zustand muss ein anderes Objekt sein</li>
          <li><code>useState</code> kann unterschiedlich aufgerufen werden
            <ul>
              <li><code>useState(value)</code>: Hat den Typ von <code>value</code></li>
              <li><code>useState(() =&gt; value)</code>: Hat den Typ von <code>value</code></li>
              <li><code>useState&lt;T&gt;()</code>: Hat den Typ <code>T | undefined</code></li>
            </ul>
          </li>
          <li><u>Nicht direkt den Wert verändern; das hat keinen Effekt</u></li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default StateTheory;