import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Context: NextPage = () => {
  return (
    <>
      <Typography variant="h1">useContext</Typography>
      <Sandbox src="https://codesandbox.io/embed/context-0rbdm?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Context;