import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const FirstComponent: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Erste Komponente</Typography>
      <Sandbox src="https://codesandbox.io/embed/firstcomponent-9goyi?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default FirstComponent;