import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Ref: NextPage = () => {
  return (
    <>
      <Typography variant="h1">ref</Typography>
      <Sandbox src="https://codesandbox.io/embed/ref-e6xoe?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Ref;