import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const If: NextPage = () => {
  return (
    <>
      <Typography variant="h1">If und For</Typography>
      <Sandbox src="https://codesandbox.io/embed/if-4pw70?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default If;