import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Columns} from "../../components/common/Presentation";


export const Overview: NextPage = () => {
  return (
    <>
      <Typography variant="h1">React</Typography>
      <Columns>
        <ul>
          <li>Entwickelt von Facebook seit 2013</li>
          <li>Anwendungen setzen sich Baumartig aus Komponenten zusammen</li>
          <li>Komponenten können DOM, Logik und Styling enthalten</li>
        </ul>
        <img style={{height: "80vh"}} src="/img/react-market-share.png" alt="React Marktanteil" />
      </Columns>
    </>
  )
}

export default Overview;