import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const EffectExample1: NextPage = () => {
  return (
    <>
      <Typography variant="h1">useEffect</Typography>
      <Sandbox src="https://codesandbox.io/embed/effectexample1-xiqyg?expanddevtools=1&fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default EffectExample1;