import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Quiz1: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Quiz</Typography>
      <Sandbox src="https://codesandbox.io/embed/quiz3-e7zlv?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Quiz1;