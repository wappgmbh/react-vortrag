import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const StateExample: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Zustände</Typography>
      <Sandbox src="https://codesandbox.io/embed/state-cc48j?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default StateExample;