import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Properties: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Properties</Typography>
      <Sandbox src="https://codesandbox.io/embed/properties-c0ghq?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Properties;