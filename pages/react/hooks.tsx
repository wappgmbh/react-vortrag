import {NextPage} from "next";
import {Box, Typography} from "@mui/material";
import {Narrow} from "../../components/common/Frame";


export const Hooks: NextPage = () => {
  return (
    <Narrow>
      <Typography variant="h1">Hooks</Typography>
      <Box>
        <ul>
          <li>Funktionen, die mit <code>use</code> anfangen, sind Hooks</li>
          <li>Für Hooks gelten die 2 Rules of Hooks:</li>
          <li>Dürfen nur in Komponenten und anderen Hooks aufgerufen werden</li>
          <li>Müssen auf der obersten Funktionsebene aufgerufen werden
            <ul>
              <li>Hook-Aufrufe müssen pro Komponente immer in der gleichen Reihenfolge und gleich häufig passieren</li>
              <li>Nicht in <code>if</code> oder <code>for</code> aufrufen und nicht hinter <code>return</code></li>
            </ul>
          </li>
          <li>Wenn ihr Hilfsfunktionen schreibt, die Hooks nutzen, sind es selbst Hooks und müssen mit <code>use</code> anfangen</li>
        </ul>
      </Box>
    </Narrow>
  )
}

export default Hooks;