import {NextPage} from "next";
import {Box, Typography} from "@mui/material";


export const EffectTheory: NextPage = () => {
  return (
    <>
      <Typography variant="h1">useEffect</Typography>
      <Box>
        <ul>
          <li>Effekt-Funktion wird ausgeführt nachdem ...
            <ul>
              <li>... die Komponente zum Dokument hinzugefügt wurde und die Komponenten-Funktion das erste mal lief</li>
              <li>... sich eine Abhängigkeit ändert</li>
            </ul>
          </li>
          <li>Cleanup-Methode wird ausgeführt bevor ...
            <ul>
              <li>... die Effekt-Funktion ausgeführt wird, weil sich eine Abhängigkeit ändert</li>
              <li>... die Komponente aus dem Dokument entfernt wird</li>
            </ul>
          </li>
        </ul>
      </Box>
    </>
  )
}

export default EffectTheory;