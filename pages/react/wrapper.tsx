import {NextPage} from "next";
import {Typography} from "@mui/material";
import {Sandbox} from "../../components/common/Sandbox";


export const Wrapper: NextPage = () => {
  return (
    <>
      <Typography variant="h1">Wrapper-Komponenten</Typography>
      <Sandbox src="https://codesandbox.io/embed/wrapper-k6063?fontsize=14&hidenavigation=1&theme=dark" />
    </>
  )
}

export default Wrapper;